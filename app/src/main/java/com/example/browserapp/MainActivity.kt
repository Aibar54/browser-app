package com.example.browserapp

import android.content.res.Configuration
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.browserapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        binding.webView.webViewClient = WebViewClient()
        setContentView(binding.root)
        binding.button.setOnClickListener {
            try {
                var url = binding.webLink.text.toString()
                if (!url.startsWith("https://")) {
                    url = "https://$url"
                }
                binding.webView.loadUrl(url)
                supportActionBar?.title = url
            } catch (e: Exception) {
                showMessage()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.overflow_menu, menu)
        if(!isPortrait()) {
            menu.findItem(R.id.item1)?.title = "Brown"
            menu.findItem(R.id.item2)?.title = "Yellow"
            menu.findItem(R.id.item3)?.title = "Orange"
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (isPortrait()) {
            when (item.itemId) {
                R.id.item1 -> binding.button.setBackgroundColor(Color.GREEN)
                R.id.item2 -> binding.button.setBackgroundColor(Color.BLUE)
                R.id.item3 -> binding.button.setBackgroundColor(Color.parseColor("#8F00FF"))
            }
        } else {
            when (item.itemId) {
                R.id.item1 -> binding.button.setBackgroundColor(Color.parseColor("#964B00"))
                R.id.item2 -> binding.button.setBackgroundColor(Color.YELLOW)
                R.id.item3 -> binding.button.setBackgroundColor(Color.parseColor("#FFA500"))
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun isPortrait(): Boolean {
        val orientation = resources.configuration.orientation
        return orientation == Configuration.ORIENTATION_PORTRAIT
    }

    private fun showMessage() {
        Toast.makeText(this, "invalid link", Toast.LENGTH_SHORT).show()
    }
}